resource "azurerm_resource_group" "curso" {
  name     = "example-resources"
  location = "West Europe"
}

output "resource_group_name" {
  value = azurerm_resource_group.curso.name
}