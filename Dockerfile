FROM augustomoreto/devos_total:maven1

WORKDIR /app

USER root

RUN apk add --update curl

ENV TZ="America/Sao_Paulo"

ADD target/spring-petclinic-rest-3.0.2.jar /app/spring-boot.jar

ADD entrypoint.sh ./

RUN chmod +x entrypoint.sh

EXPOSE 9966

ENTRYPOINT [ "./entrypoint.sh" ]